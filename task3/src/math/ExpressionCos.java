package math;

/**
 * Created by Босс on 08.01.2017.
 */
public class ExpressionCos implements Expression{

    private final Expression expression;

    public ExpressionCos(Expression expression){
        this.expression = expression;
    }

    @Override
    public double getValue() {
        return Math.cos(expression.getValue());
    }
}