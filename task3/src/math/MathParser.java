package math;

import org.apache.log4j.Logger;

import java.util.*;

/**
 * Created by Босс on 08.01.2017.
 */
public class MathParser {

    static Logger logger = Logger.getLogger(MathParser.class);

    public static final Map<String, Integer> MATH_OPERATIONS;

    static {
        MATH_OPERATIONS = new HashMap<>();
        MATH_OPERATIONS.put("sin", 1);
        MATH_OPERATIONS.put("cos", 1);
        MATH_OPERATIONS.put("*", 2);
        MATH_OPERATIONS.put("/", 2);
        MATH_OPERATIONS.put("+", 3);
        MATH_OPERATIONS.put("-", 3);
    }


    public static String toPolishNotation(String expression) {
        String leftBracket = "(";
        String rightBracket = ")";
        if (expression == null || expression.length() == 0)
            logger.error("negative argument: ", new IllegalStateException("Expression isn't specified."));
        List<String> out = new ArrayList<>();
        Stack<String> stack = new Stack<>();
        expression = expression.replace(" ", "");

        Set<String> operationSymbols = new HashSet<>(MATH_OPERATIONS.keySet());
        operationSymbols.add(leftBracket);
        operationSymbols.add(rightBracket);

        int index = 0;
        boolean findNext = true;
        while (findNext) {
            int nextOperationIndex = expression.length();
            String nextOperation = "";
            for (String operation : operationSymbols) {
                int i = expression.indexOf(operation, index);
                if (i >= 0 && i < nextOperationIndex) {
                    nextOperation = operation;
                    nextOperationIndex = i;
                }
            }

            if (nextOperationIndex == expression.length()) {
                findNext = false;
            }
            else {
                if (index != nextOperationIndex) {
                    out.add(expression.substring(index, nextOperationIndex));
                }
                if (nextOperation.equals(leftBracket)) {
                    stack.push(nextOperation);
                }
                else if (nextOperation.equals(rightBracket)) {
                    while (!stack.peek().equals(leftBracket)) {
                        out.add(stack.pop());
                        if (stack.empty()) {
                            logger.error("negative argument: ", new IllegalArgumentException("Unmatched brackets"));
                        }
                    }
                    stack.pop();
                }
                else {
                    while (!stack.empty() && !stack.peek().equals(leftBracket) &&
                            (MATH_OPERATIONS.get(nextOperation) >= MATH_OPERATIONS.get(stack.peek()))) {
                        out.add(stack.pop());
                    }
                    stack.push(nextOperation);
                }
                index = nextOperationIndex + nextOperation.length();
            }
        }
        if (index != expression.length()) {
            out.add(expression.substring(index));
        }
        while (!stack.empty()) {
            out.add(stack.pop());
        }
        String result = "";
        if (!out.isEmpty())
            result += out.remove(0);
        while (!out.isEmpty())
            result +=" " + out.remove(0);

        return result;
    }
}
