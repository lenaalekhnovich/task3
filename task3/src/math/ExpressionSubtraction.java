package math;

public class ExpressionSubtraction implements Expression{
	
	private final Expression leftExpression;
	private final Expression rightExpression;

	public ExpressionSubtraction(Expression leftExpression, Expression rightExpression ){
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	@Override
	public double getValue() {
		return leftExpression.getValue() - rightExpression.getValue();
	}

}
