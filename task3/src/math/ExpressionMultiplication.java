package math;

public class ExpressionMultiplication implements Expression{

	private final Expression leftExpression;
	private final Expression rightExpression;

	public ExpressionMultiplication(Expression leftExpression, Expression rightExpression ){
		this.leftExpression = leftExpression;
		this.rightExpression = rightExpression;
	}

	@Override
	public double getValue() {
		return leftExpression.getValue() * rightExpression.getValue();
	}
}
