package math;

/**
 * Created by Босс on 08.01.2017.
 */
public class ExpressionDivision implements Expression{

    private final Expression leftExpression;
    private final Expression rightExpression;

    public ExpressionDivision(Expression leftExpression, Expression rightExpression ){
        this.leftExpression = leftExpression;
        this.rightExpression = rightExpression;
    }

    @Override
    public double getValue() {
        return leftExpression.getValue() * rightExpression.getValue();
    }
}
