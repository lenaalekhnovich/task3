package math;

/**
 * Created by Босс on 08.01.2017.
 */
public class ExpressionSin implements Expression{

    private final Expression expression;

    public ExpressionSin(Expression expression){
        this.expression = expression;
    }

    @Override
    public double getValue() {
        return Math.sin(expression.getValue());
    }
}