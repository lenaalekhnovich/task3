package math;

import java.util.Stack;

public class Counter {

	public static String countExpression(String expressionStr) {
		double result = 0;
		Stack<Expression> stack = new Stack<>();
		String[] expressionArray = expressionStr.split(" ");
		for (String symbol : expressionArray) {
			if (ExpressionProperty.isOperator(symbol)) {
				Expression leftExpression = stack.pop();
				Expression rightExpression = stack.pop();
				Expression operation = ExpressionProperty.getOperator(symbol, leftExpression, rightExpression);
				result = operation.getValue();
				stack.push(new ExpressionNumber(result));
			} else if(ExpressionProperty.isTrigonometricOperator(symbol)){
				Expression expression = stack.pop();
				Expression operation = ExpressionProperty.getOperator(symbol, expression, expression);
				result = operation.getValue();
				stack.push(new ExpressionNumber(result));
			}
			else if(symbol.equals("PI")) {
					Expression number = new ExpressionNumber(Math.PI);
					stack.push(number);
			}
			else {
					Expression number = new ExpressionNumber(Double.parseDouble(symbol));
					stack.push(number);
			}
		}
		return Integer.toString((int)stack.pop().getValue());
	}
}
