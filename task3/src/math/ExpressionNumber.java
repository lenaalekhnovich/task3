package math;

public class ExpressionNumber implements Expression{

	private final double number;
	
	public ExpressionNumber(double number){
		this.number = number;
	}

	@Override
	public double getValue() {
		return number;
	}
}
