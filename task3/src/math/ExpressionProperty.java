package math;

public class ExpressionProperty {

	public static boolean isOperator(String str) {
		return (str.equals("+") || str.equals("-") || str.equals("*") || str.equals("/"));
	}

	public static boolean isTrigonometricOperator(String str){
		return str.equals("cos")|| str.equals("sin");
	}
	
	public static Expression getOperator(String operator, Expression leftExpression, Expression rightExpression) {
		Expression expression = null;
		switch (operator) {
			case "+":
				return new ExpressionAddition(leftExpression, rightExpression);
			case "-":
				return new ExpressionSubtraction(leftExpression, rightExpression);
			case "*":
				return new ExpressionMultiplication(leftExpression, rightExpression);
			case "/":
				return new ExpressionDivision(leftExpression, rightExpression);
			case "sin":
				return new ExpressionSin(leftExpression);
			case "cos":
				return new ExpressionCos(leftExpression);
		}
		return expression;
	}

}
