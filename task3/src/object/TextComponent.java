package object;

/**
 * Created by Босс on 07.01.2017.
 */
public interface TextComponent {

    void add(TextComponent... obj);
    String get();
    String getComponent(int indexSentence, int indexLexeme);
    int getSize();
}
