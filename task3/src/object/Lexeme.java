package object;

import org.apache.log4j.Logger;

/**
 * Created by Босс on 07.01.2017.
 */
public class Lexeme implements TextComponent{

    static Logger logger = Logger.getLogger(Lexeme.class);

    private String lexeme = "";

    public Lexeme(String lexeme) {
        this.lexeme = lexeme;
    }

    @Override
    public void add(TextComponent... obj) {
        logger.error("negative argument: ", new IllegalStateException("Impossible to add component"));
    }

    @Override
    public String get() {
        return lexeme + " ";
    }

    @Override
    public String getComponent(int indexSentence, int indexLexeme) {
        return lexeme;
    }

    @Override
    public int getSize() {
        return 1;
    }
}
