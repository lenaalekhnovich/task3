package object;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 06.01.2017.
 */
public class Sentence implements TextComponent{
    private List<TextComponent> listLexeme = null;

    public Sentence(){
        listLexeme = new LinkedList<>();
    }

    @Override
    public void add(TextComponent... lexeme) {
        for (TextComponent textComponent: lexeme) {
            listLexeme.add(textComponent);
        }
    }

    @Override
    public String get() {
        String sentence = "";
        for (int i = 0; i < listLexeme.size(); i++) {
            sentence += listLexeme.get(i).get();
        }
        return sentence;
    }

    @Override
    public String getComponent(int indexSentence, int indexLexeme) {
        return listLexeme.get(indexLexeme).getComponent(indexSentence,indexLexeme);
    }

    @Override
    public int getSize() {
        return listLexeme.size();
    }
}
