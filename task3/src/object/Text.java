package object;

import java.util.LinkedList;
import java.util.List;

/**
 * Created by Босс on 07.01.2017.
 */
public class Text implements TextComponent {
    private List<TextComponent> listSentence = null;

    public Text(){
        listSentence = new LinkedList<>();
    }

    @Override
    public void add(TextComponent... sentence) {
        for (TextComponent textComponent: sentence) {
            listSentence.add(textComponent);
        }
    }

    @Override
    public String get() {
        String text = "";
        for (int i = 0; i < listSentence.size(); i++) {
            text += listSentence.get(i).get();
        }
        return text;
    }

    @Override
    public String getComponent(int indexSentence, int indexLexeme) {
        return listSentence.get(indexSentence).getComponent(indexSentence, indexLexeme);
    }

    @Override
    public int getSize() {
        return listSentence.size();
    }

    public List<TextComponent> getListSentence() {
        return listSentence;
    }
}
