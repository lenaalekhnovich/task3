package parsing;

import object.Text;

/**
 * Created by Босс on 08.01.2017.
 */
public class TextParser {

    public Text parseText(String textStr){
        LexemeParser lexemeParser = new LexemeParser();
        SentenceParser sentenceParser = new SentenceParser(textStr, lexemeParser);
        Text text = sentenceParser.parse();
        return text;
    }
}
