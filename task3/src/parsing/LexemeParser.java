package parsing;

import math.Counter;
import math.MathParser;
import object.Lexeme;
import object.Sentence;
import object.Text;

import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;



/**
 * Created by Босс on 08.01.2017.
 */
public class LexemeParser implements Parser {

    private List<String> listSentence = null;

    public LexemeParser(){
        listSentence = new LinkedList<>();
    }

    public void addSentence(String... sentence){
        for (String str: sentence) {
            listSentence.add(str);
        }
    }

    @Override
    public Text parse() {
        Text text = new Text();
        Sentence sentence;
        String regex = "([^\\s]+[.\\s])";
        Pattern p = Pattern.compile(regex);
        for(int i = 0; i < listSentence.size(); i++) {
            sentence = new Sentence();
            Matcher m = p.matcher(listSentence.get(i));
            while (m.find()) {
                String lexeme = checkMathExpression(m.group()).trim();
                sentence.add(new Lexeme(lexeme));
            }
            text.add(sentence);
        }
        return text;
    }

    public String checkMathExpression(String lexeme){
        String regex = "[0-9][\\+\\-\\*\\/]|sin\\(.+\\)|cos\\(.+\\)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(lexeme);
        if(m.find()){
            lexeme = lexeme.replaceAll("[Pp][Ii]", "PI");
            lexeme = MathParser.toPolishNotation(lexeme);
            lexeme = Counter.countExpression(lexeme);
        }
        return lexeme;
    }
}
