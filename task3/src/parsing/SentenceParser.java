package parsing;

import object.Text;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Босс on 08.01.2017.
 */
public class SentenceParser implements Parser{

    private LexemeParser lexemeParser  = null;
    private String text = "";

    public SentenceParser(String text, LexemeParser lexemeParser){
        this.text = text;
        this.lexemeParser = lexemeParser;
    }


    @Override
    public Text parse() {
        String regex = "([A-ZА-Я][^.]+\\.)";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(text);
        while(m.find()){
            lexemeParser.addSentence(m.group());
        }
        if(lexemeParser == null) {
            return new Text();
        }
        else {
            return lexemeParser.parse();
        }

    }
}
