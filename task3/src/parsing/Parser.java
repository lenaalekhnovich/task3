package parsing;

import object.Text;

/**
 * Created by Босс on 07.01.2017.
 */
public interface Parser {

    public Text parse();
}
