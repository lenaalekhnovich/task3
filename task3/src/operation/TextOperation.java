package operation;

import object.Lexeme;
import object.Text;

import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by Босс on 08.01.2017.
 */
public class TextOperation {

    public int findSentencesWithSameWords(Text text){
        int maxAmount = 0;
        for(int i = 0 ; i < text.getSize() ; i++){
            for(int j = 0; j < text.getListSentence().get(i).getSize(); j++) {
                String word = text.getComponent(i, j).replace("[.,!?]", "");
                int size = checkWord(text, word, i + 1);
                if(size > maxAmount){
                    maxAmount = size;
                }
            }
        }
        return maxAmount;
    }

    public int checkWord(Text text, String compareWord, int indexBegin){
        int size = 1;
        for(int i = indexBegin; i < text.getSize(); i++){
            for(int j = 0; j < text.getListSentence().get(i).getSize(); j++){
                String word = text.getComponent(i, j).replaceAll("[.'',!?]", "");
                if(word.equalsIgnoreCase(compareWord)){
                    size++;
                    break;
                }
            }
        }
        return size;
    }

    public List<Lexeme> sortByName(Text text){
        List<Lexeme> listLexemes = getTextLexemes(text);
        Collections.sort(listLexemes, new Comparator<Lexeme>() {
            public int compare(Lexeme o1, Lexeme o2) {
                return o1.get().compareToIgnoreCase(o2.get());
            }
        });
        for(int i = 0; i < listLexemes.size(); i++) {
            String lexeme = listLexemes.get(i).get();
            String regex = "([A-ZА-Я].+)";
            Pattern p = Pattern.compile(regex);
            Matcher m = p.matcher(lexeme);
            lexeme = m.find() ? "\n" + lexeme : lexeme;
            listLexemes.set(i, new Lexeme(lexeme));
        }
        return listLexemes;
    }

    public List<Lexeme> sortByAmount(Text text, char symbol){
        List<Lexeme> listLexemes = getTextLexemes(text);
        Collections.sort(listLexemes, new ComparatorByAmount(symbol));
        return listLexemes;
    }

    public List<Lexeme> getTextLexemes(Text text){
        List<Lexeme> listLexemes = new LinkedList<>();
        for(int i = 0; i < text.getSize(); i++){
            for(int j = 0; j < text.getListSentence().get(i).getSize(); j++){
                String lexeme = text.getComponent(i, j).replaceAll("[.'',!?]", "");
                listLexemes.add(new Lexeme(lexeme));
            }
        }
        return listLexemes;
    }

    public static int getSymbolAmount(Lexeme lexeme, char symbol){
        int amountSymbol = 0;
        String lexemeStr = lexeme.get();
        String regex = "[" + symbol + Character.toUpperCase(symbol) + "]";
        Pattern p = Pattern.compile(regex);
        Matcher m = p.matcher(lexemeStr);
        while(m.find()){
            amountSymbol++;
        }
        return amountSymbol;
    }

}
