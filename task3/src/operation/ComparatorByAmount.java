package operation;

import object.Lexeme;

import java.util.Comparator;

/**
 * Created by Босс on 09.01.2017.
 */
public class ComparatorByAmount implements Comparator<Lexeme>{

    private char symbol;

    public ComparatorByAmount(char symbol){this.symbol = symbol;}

    @Override
    public int compare(Lexeme o1, Lexeme o2) {
        int result = Integer.toString(TextOperation.getSymbolAmount(o1, symbol)).compareTo
                (Integer.toString(TextOperation.getSymbolAmount(o2, symbol)));
        if (result != 0)
            return result/Math.abs(result);
        result = o1.get().compareToIgnoreCase(o2.get());
        return (result != 0) ? result/Math.abs(result) : 0;
    }
}
