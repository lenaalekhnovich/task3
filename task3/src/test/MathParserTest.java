package test;

import math.Counter;
import math.MathParser;
import org.junit.Assert;
import org.junit.Test;

import java.util.EmptyStackException;

/**
 * Created by Босс on 09.01.2017.
 */
public class MathParserTest {

    @Test
    public void polishNotationTest() {
        String example = "(1+2)*4+3";
        String rightResult = "1 2 + 4 * 3 +";
        String result = MathParser.toPolishNotation(example);
        Assert.assertEquals(rightResult, result);
    }

    @Test (expected = EmptyStackException.class)
    public void wrongPolishNotationTest(){
        String example = "(((1+2)*4)+4+)))";
        MathParser.toPolishNotation(example);
    }

    @Test
    public void countExpressionTest(){
        String example = "sin(180)+(1-2*345) -4 / 2";
        String result = Counter.countExpression(MathParser.toPolishNotation(example));
        Assert.assertNotNull(result);
    }

    @Test
    public void checkCorrectCounterTest(){
        String example = "2*(2+2*5)";
        String rightResult = "24";
        String result = Counter.countExpression(MathParser.toPolishNotation(example));
        Assert.assertEquals(rightResult, result);
    }

}
