package test;

import file.FileWork;
import object.Lexeme;
import object.Sentence;
import object.Text;
import org.junit.Assert;
import org.junit.Test;
import parsing.TextParser;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;

/**
 * Created by Босс on 09.01.2017.
 */
public class TextParserTest {

    @Test
    public void checkTextObjectTest(){
        Text text = new Text();
        Sentence sentence= new Sentence();
        sentence.add(new Lexeme("I"), new Lexeme("like"), new Lexeme("dogs"));
        text.add(sentence);
        String rightResult = "like";
        Assert.assertEquals(text.getComponent(0,1), rightResult);
    }

    @Test
    public void readFromFileTest() throws FileNotFoundException, UnsupportedEncodingException {
        FileWork file = new FileWork("src/resources/file.txt");
        String text = file.readFromFile();
        Assert.assertNotNull(text);
    }

    @Test
    public void parseTextTest(){
        String textStr = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse et diam ipsum.";
        TextParser parser = new TextParser();
        Text text = parser.parseText(textStr);
        Assert.assertNotNull(text);
    }

    @Test
    public void makeTextFromObjectTest(){
        String textStr = "I live 2+3 in Belarus.";
        TextParser parser = new TextParser();
        Text text = parser.parseText(textStr);
        String result = "I live 5 in Belarus.";
        Assert.assertEquals(text.get().trim(), result);
    }
}
