package test;

import file.FileWork;
import object.Lexeme;
import object.Text;
import operation.TextOperation;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import parsing.TextParser;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.List;

/**
 * Created by Босс on 09.01.2017.
 */
public class TextOperationTest {

    private static TextOperation textOperation;
    private static FileWork fileWork;
    private static Text text;

    @BeforeClass
    public static void init() throws FileNotFoundException, UnsupportedEncodingException {
        textOperation = new TextOperation();
        fileWork = new FileWork("src/resources/file.txt");
    }

    @BeforeClass
    public static void readParseTextTest() throws IOException {
        TextParser parser = new TextParser();
        String textStr = fileWork.readFromFile();
        text = parser.parseText(textStr);
        Assert.assertNotNull(text);
    }

    @Test
    public void makeTextFromParseTextObject(){
        String textStr = text.get();
        Assert.assertNotNull(textStr);
    }

    @Test
    public void findSentencesTest(){
        int amount = textOperation.findSentencesWithSameWords(text);
        Assert.assertNotEquals(amount, 0);
    }

    @Test
    public void sortByNameTest(){
        List<Lexeme> list = textOperation.sortByName(text);
        for (int i = 0; i < list.size(); i++){
            System.out.print(list.get(i).get());
        }
        Assert.assertNotNull(list);
    }

    @Test
    public void sortByAmountTest(){
        List<Lexeme> list = textOperation.sortByAmount(text, 'a');
        Assert.assertNotNull(list);
    }

}
