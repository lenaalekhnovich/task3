package file;

import org.apache.log4j.Logger;

import java.io.*;

/**
 * Created by Босс on 18.12.2016.
 */
public class FileWork {
    private FileInputStream inF;
    private BufferedReader reader;

    static Logger logger = Logger.getLogger(FileWork.class);

    public FileWork(String fileName) throws FileNotFoundException, UnsupportedEncodingException {
        inF = new FileInputStream(fileName);
        reader = new BufferedReader(new InputStreamReader(inF, "UTF-8"));
    }

    public String readFromFile() {
        String text = "";
        String str;
        try {
            while((str = reader.readLine()) != null){
                text += str;
            }
        } catch (IOException e) {
            logger.error("negative argument: ", e);
        }
        return text;
    }


}
